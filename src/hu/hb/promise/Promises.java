package hu.hb.promise;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

class Promises {

    //If threads were non-daemon, the application wouldn't terminate when the task queue is empty
    private static final ExecutorService executor = Executors.newCachedThreadPool(new DaemonThreadFactory());


    static <V> void subscribe(Promise<V> promise) {
        executor.submit(() -> promise.resolver.execute(promise.onResolve(), promise.onReject()));
    }

    private static class DaemonThreadFactory implements ThreadFactory {

        @Override
        public Thread newThread(Runnable r) {
            Thread t = new Thread(r);
            t.setDaemon(true);
            return t;
        }
    }

}
