package hu.hb.promise.example;

import hu.hb.promise.Promise;

import java.io.PrintStream;

public class Main {


    public static void main(String[] args) throws Exception {
        Promise<Integer> promiseChain = new Promise<Integer>(
                ((resolve, reject) -> {
                    print("p1");
                    new Thread(() -> {
                        try {
                            Thread.sleep(500);
                            resolve.accept(1);
                            //reject.accept(new Exception());
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }).start();
                }))
                .then(value -> {
                    print(value);
                    return value + " 2";
                })
                .then(value -> value + "-" + value)
                .then(value -> {
                    throw new IllegalArgumentException("Something happened!!");
                })
                .then((value) -> {
                    print(value);
                    return null;
                })
                .catchh(e -> {
                    print("Exception: " + e.getMessage(), System.err);
                    return -1;
                });

        print(promiseChain.await());
    }

    private static void print(Object message) {
        print(message, System.out);
    }

    private static void print(Object message, PrintStream stream) {
        stream.printf("%-10s %s\n", Thread.currentThread().getName(), message.toString());
    }

}
