package hu.hb.promise;

import java.util.function.Consumer;

public interface OnResolve<T> extends Consumer<T> {
}
