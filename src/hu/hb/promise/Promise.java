package hu.hb.promise;

public class Promise<V> {

    final Resolver<V> resolver;

    private Promise<?> then;
    private Promise<?> catchh;

    private State state = State.PENDING;

    private V value;
    private Exception exception;

    public Promise(Resolver<V> resolver) {
        this(resolver, true);
    }

    private Promise(Resolver<V> resolver, boolean subscribe) {
        this.resolver = resolver;
        if (subscribe)
            Promises.subscribe(this);
    }

    public <N> Promise<N> then(Action<V, N> action) {
        synchronized (this) {
            Promise<N> _then = new Promise<>((resolve, reject) -> {
                try {
                    resolve.accept(action.execute(value));
                } catch (Exception e) {
                    reject.accept(e);
                }
            }, state != State.PENDING); // If this promise is resolved or rejected, schedule immediately
            then = _then;
            return _then;
        }
    }

    public <N> Promise<N> catchh(Action<Exception, N> action) {
        synchronized (this) {
            Promise<N> _catch = new Promise<>((resolve, reject) -> {
                try {
                    resolve.accept(action.execute(exception));
                } catch (Exception e) {
                    reject.accept(e);
                }
            }, state != State.PENDING); // If this promise is resolved or rejected, schedule immediately
            catchh = _catch;
            return _catch;
        }
    }

    OnResolve<V> onResolve() {
        return v -> {
            synchronized (Promise.this) {
                setState(State.RESOLVED);
                value = v;
                if (then != null) {
                    Promises.subscribe(then);
                }
            }
        };
    }

    OnReject onReject() {
        return e -> {
            synchronized (Promise.this) {
                setState(State.REJECTED);
                exception = e;
                Promise<?> promise = this;
                while (true) {
                    synchronized (promise) {
                        if (promise.then == null)
                            break;
                        if (promise.catchh != null) {
                            promise.exception = exception;
                            Promises.subscribe(promise.catchh);
                            return;
                        }
                        promise = promise.then;
                    }
                }

                if (promise.catchh != null) {
                    promise.exception = exception;
                    Promises.subscribe(promise.catchh);
                }
            }
        };
    }

    synchronized void setState(State state) {
        this.state = state;
        notifyAll();
    }

    public synchronized State getState() {
        return state;
    }

    public V await() throws Exception {
        synchronized (this) {

            if (state == State.PENDING)
                wait();

            if (state == State.RESOLVED)
                return value;
            else if (state == State.REJECTED)
                throw exception;
            else
                throw new AssertionError("Should not reach this branch");
        }
    }

    enum State {
        PENDING, RESOLVED, REJECTED
    }

}
