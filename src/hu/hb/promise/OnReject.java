package hu.hb.promise;

import java.util.function.Consumer;

public interface OnReject extends Consumer<Exception> {
}
